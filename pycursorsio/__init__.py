#!/usr/bin/env python3
# *****************************************************************************
# Copyright (C) 2021 Thomas Touhey <thomas@touhey.fr>
# This file is part of the pycursorsio project, which is MIT-licensed.
# *****************************************************************************
"""Python client for cursors.io, using pygame and websockets."""

__version__ = '2022103101'
