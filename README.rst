pycursorsio -- a Python client for cursors.io
=============================================

This module is a Python graphical client for `cursors.io`_ made by
`Thomas Touhey`_. It contains efforts to:

- reverse engineer the cursors.io protocol.
- reimplement the client-side application.
- experiment with pygame, websockets and asyncio.

It was initially made in a rush in the first week of November 2020.

.. _cursors.io: http://cursors.io/
.. _Thomas Touhey: https://thomas.touhey.fr/
