#!/usr/bin/make -f
install-and-run: install run  # default rule

install:
	@poetry install

run:
	@poetry run python -m pycursorsio

dist: $(DNAME)
$(DNAME):
	@poetry build

upload: $(DNAME)
	@poetry upload

.PHONY: install run install-and-run dist upload
